"""
HackerRank - Cut the sticks

www.hackerrank.com/challenges/crush/problem
"""


def arrayManipulation(entries, queries):
    """ Instead of adding the query value to all the entries
        of the array between start and end and finding the max,
        adding the query value to the first (start) and subtracting
        it after the end (end + 1) and then finding the max gain has
        the same effect.
        Hint: think super mario growing from mushroom to mushroom.
        We want to know what was his biggest height. Subtracting
        after the end, is like simulating the shrinking after eating
        the mushroom. The numbers remaining in the array is the total
        gain Mario got which corresponds to the max height he achieved.

        To make it work instead of start and (end + 1), start - 1 and end
        was used.
    """
    array = [0] * entries

    for query in queries:
        start = query[0]
        end = query[1]
        summard = query[2]

        if start-1 < entries:
            array[start - 1] += summard
        if end < entries:
            array[end] -= summard

        # print(array)

    maxgain = 0
    gain = 0
    for num in array:
        gain += num
        if gain > maxgain:
            maxgain = gain
    print(maxgain)
    return maxgain


if __name__ == '__main__':
    with open("input2", 'r') as file:
        nm = file.readline().split()
        n = int(nm[0])
        m = int(nm[1])

        queries = []

        for _ in range(m):
            nextq = list(map(int, file.readline().rstrip().split()))
            queries.append(nextq)

        result = arrayManipulation(n, queries)
