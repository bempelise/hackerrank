"""
HackerRank - Factorial digit sum

https://www.hackerrank.com/contests/projecteuler/challenges/euler020/problem

n! means n*(n - 1) * ... * 3 * 2 * 1

For example, 10! = 10*9*8*7*6*5*4*3*2*1 = 3628800,
and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.

Find the sum of the digits in the number N!

"""

import sys

FACTORIAL = []


def problem_name(number):
    """ Solves the problem"""
    fact = FACTORIAL[number]
    summation = 0
    while fact > 0:
        summation += fact % 10
        fact = fact // 10
    return summation


def cache_factorial():
    """ Caches the factorial values """
    for i in range(1000):
        if i < 2:
            FACTORIAL.append(1)
        else:
            FACTORIAL.append(FACTORIAL[i - 1] * i)


def hackerank_std():
    """ hackerank standard io """
    size = int(sys.stdin.readline().strip().split(' ')[0])
    for x in range(int(size)):
        number = int(sys.stdin.readline().strip().split(' ')[0])
        res = problem_name(number)
        sys.stdout.write(str(res) + '\n')


def main():
    """ main """
    cache_factorial()
    print(problem_name(10))     #27


if __name__ == "__main__":
    main()
