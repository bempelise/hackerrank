"""
HackerRank - Counting Sundays

https://www.hackerrank.com/contests/projecteuler/challenges/euler019/problem

You are given the following information,
but you may prefer to do some research for yourself.

1 Jan 1900 was a Monday.
Thirty days has September,
April, June and November.
All the rest have thirty-one,
Saving February alone,
Which has twenty-eight, rain or shine.
And on leap years, twenty-nine.

A leap year occurs on any year evenly divisible by 4,
but not on a century unless it is divisible by 400.

How many Sundays fell on the first of
the month between two dates(both inclusive)?

***
More info:
https://calendars.fandom.com/wiki/Zeller's_congruence

"""
import sys

SUN = 0
MON = 1
TUE = 2
WED = 3
THU = 4
FRI = 5
SAT = 6


class Date():
    def __init__(self, year, month, day):
        self.year = year
        self.month = month
        self.day = day

    def earlier_than(self, other):
        if self.year > other.year:
            return False
        if self.year == other.year:
            if self.month > other.month:
                return False
            if self.month == other.month:
                if self.day > other.day:
                    return False
        return True

    def equals(self, other):
        return self.year == other.year and self.month == other.month and self.day == other.day

    def dotw(self):
        """ Returns the day of the week for the given date
            h initially has values: 0 = Saturday, 1 = Sunday, 2 = Monday, ..., 6 = Friday
            Normalize gets it to 1 = Monday to 7 = Sunday
        """
        day = self.day
        month = self.month
        year = self.year

        if month < 3:
            month += 12
            year -= 1
        dv = year // 100
        y = year % 100
        d = (y + (y // 4) + (dv // 4) - (2 * dv) + ((26 * (month + 1)) // 10) + day - 1) % 7
        return d


def counting_sundays(start_year, start_month, start_day, end_year, end_month, end_day):
    """ Solves the problem"""
    count = 0
    start = Date(start_year, start_month, start_day)
    end = Date(end_year, end_month, end_day)

    if end.earlier_than(start):
        end = Date(start_year, start_month, start_day)
        start = Date(end_year, end_month, end_day)

    while start.earlier_than(end):
        if (start.day == 1 and start.dotw() == SUN):
            count += 1

        start.day = 1
        start.month += 1
        if start.month > 12:
            start.month = 1
            start.year += 1

    if start.equals(end):
        if (start.day == 1 and start.dotw() == SUN):
            count += 1

    return count


def hackerank_std():
    """ hackerank std i/o """
    size = int(sys.stdin.readline().strip().split(' ')[0])
    for x in range(int(size)):
        start = sys.stdin.readline().strip().split(' ')
        end = sys.stdin.readline().strip().split(' ')

        res = counting_sundays(int(start[0]),
                               int(start[1]),
                               int(start[2]),
                               int(end[0]),
                               int(end[1]),
                               int(end[2]))
        sys.stdout.write(str(res) + '\n')


def main():
    """ main """
    print(counting_sundays(1900, 1, 1, 1910, 1, 1))     # 18
    print(counting_sundays(4699, 12, 12, 4710, 1, 1))   # 18
    print(counting_sundays(1988, 3, 25, 1989, 7, 13))   # 2
    print(counting_sundays(1924, 6, 6, 1925, 6, 16))    # 2
    print(counting_sundays(1000000000000, 2, 2, 1000000001000, 3, 2)) # 1720
    print(counting_sundays(1925, 6, 16, 1924, 6, 6))    # 2
    print(counting_sundays(1905, 1, 1, 1905, 1, 1))     # 1


if __name__ == "__main__":
    main()
