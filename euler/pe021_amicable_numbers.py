"""
HackerRank - Amicable Numbers

https://www.hackerrank.com/contests/projecteuler/challenges/euler021/problem?isFullScreen=true

Let d(n) be defined as the sum of proper divisors of n
(numbers less than n which divide evenly into n).

If d(a) = b and d(b) = a, where a != b, then a and b
are an amicable pair and each of a and b are called amicable numbers.

For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 and 110
therefore d(220) = 284.
The proper divisors of 284 are 1, 2, 4, 71 and 142 so d(284) = 220.

Evaluate the sum of all the amicable numbers under N.

"""
import sys
import math

DELTA = {}

AMICABLE = [
    220,
    284,
    1184,
    1210,
    2620,
    2924,
    5020,
    5564,
    6232,
    6368,
    10744,
    10856,
    12285,
    14595,
    17296,
    18416,
    63020,
    66928,
    66992,
    67095,
    69615,
    71145,
    76084,
    79750,
    87633,
    88730
]


def delta(number):
    """ returns sum of the divisors of given number """

    if number in DELTA.keys():
        return DELTA[number]

    summation = 1

    for index in range(2, int(math.sqrt(number)) + 1):
        if number % index == 0:
            summation += index
    DELTA[number] = summation
    return summation


def is_amicable(number):
    """ returns true if number is amicable """
    other = delta(number)
    if other != number and delta(other) == number:
        return True


def amicable_numbers(limit):
    """ Solves the problem"""
    summation = 0

    for number in AMICABLE:
        if number > limit:
            break
        summation += number

    return summation


def hackerank_std():
    """ hackerank standard io """
    size = int(sys.stdin.readline().strip().split(' ')[0])
    for x in range(int(size)):
        number = int(sys.stdin.readline().strip().split(' ')[0])
        res = amicable_numbers(number)
        sys.stdout.write(str(res) + '\n')


def main():
    """ main """
    # print(delta(220))
    # print(delta(284))
    for i in range(100000):
        if (is_amicable(i)):
            print(i)
    # print(amicable_numbers(300)) #504


if __name__ == "__main__":
    main()

# NOTES:
# Presumably there is away to beat the time in the last
# test case without using precalculated values by using
# the functions below for delta. The first one assumes
# precalculation of primes using a sieve.


def fast_sum_of_proper_divisors(n, primes, is_prime):
    """
    input n, a list of primes, and a list of booleans
    returns the sum of the proper divisors
    """
    if is_prime[n]:
        return 1
    n_copy = n
    product = 1
    for p in primes:
        if is_prime[n]:
            product *= (n**(1 + 1) - 1)//(n - 1)
            break
        if p > n:
            break
        i = 0
        while n % p == 0:
            n //= p
            i += 1
        if i != 0:
            product *= (p**(i + 1) - 1)//(p - 1)
    return product - n_copy



def getDivSum(n):
    res = 0
    for i in range(2, (int)(math.sqrt(n))+1):
        if n % i == 0:
            if i == (n / i):
                res += i
            else:
                res += i + n / i
    res += 1
    return int(res)
