"""
HackerRank - Problem Name

https://www.hackerrank.com/problem_URL

Problem description
"""
import sys


def problem_name():
    """ Solves the problem"""


def main():
    """ main """
    print(problem_name())


def hackerank_std():
    """ hackerank standard io """
    size = int(sys.stdin.readline().strip().split(' ')[0])


if __name__ == "__main__":
    main()
