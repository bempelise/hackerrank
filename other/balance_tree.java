    /* Class node is defined as :
    class Node 
        int val;    //Value
        int ht;     //Height
        Node left;  //Left child
        Node right; //Right child

    */

    static Node insert(Node root,int val)
    {
        if (val > root.val)
        {
            if (root.left == null)
            {
                root.left = new Node();
                root.left.val = val
            }
            insert(root.left, val);
        }
        else
        {
            if (root.right == null)
            {
                root.right = new Node();
                root.right.val = val
            }
            insert(root.right, val);
        }
    }
